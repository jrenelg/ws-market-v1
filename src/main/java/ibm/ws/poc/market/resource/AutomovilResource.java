package ibm.ws.poc.market.resource;

import java.net.URI;
import java.util.List;

import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import ibm.ws.poc.market.model.*;
import ibm.ws.poc.market.resource.bean.AutomovilFilterBean;
import ibm.ws.poc.market.service.*;

@Path("/automovil")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AutomovilResource {
	
	AutomovilService automovilService = new AutomovilService();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Automovil> getJsonAutomoviles(@BeanParam AutomovilFilterBean filterBean) {
		System.out.println("JSON CALL");
		if (filterBean.getYear() > 0) {
			return automovilService.getAllAutomovilForYear(filterBean.getYear());
		}
		if (filterBean.getStart() >= 0 && filterBean.getSize() > 0) {
			return automovilService.getAllAutomovilPaginated(filterBean.getStart(), filterBean.getSize());
		}
		return automovilService.getAllAutomovil();
	}

	@GET
	@Produces(MediaType.TEXT_XML)
	public List<Automovil> getXmlAutomoviles(@BeanParam AutomovilFilterBean filterBean) {
		System.out.println("XML CALL");
		if (filterBean.getYear() > 0) {
			return automovilService.getAllAutomovilForYear(filterBean.getYear());
		}
		if (filterBean.getStart() >= 0 && filterBean.getSize() > 0) {
			return automovilService.getAllAutomovilPaginated(filterBean.getStart(), filterBean.getSize());
		}
		return automovilService.getAllAutomovil();
	}
	
	@GET
	@Path("/{automovilId}")
	public Automovil getAutomovil(@PathParam("automovilId") long id, @Context UriInfo uriInfo) {
		Automovil automovil = automovilService.getAutomovil(id);
		automovil.addLink(getUriForSelf(uriInfo, automovil), "self");
		automovil.addLink(getUriForComments(uriInfo, automovil), "comments");
		return automovil;

	}
	
	@POST
	public Response postAutomovil(Automovil automovil, @Context UriInfo uriInfo) {
		Automovil newAutomovil = automovilService.addAutomovil(automovil);
		String newId = String.valueOf(newAutomovil.getId());
		//uriInfo.getAbsolutePath()
		URI uri = uriInfo.getAbsolutePathBuilder().path(newId).build();
		/*return Response.status(Status.CREATED)
				.entity(newAutomovil)
				.build();*/
		return Response.created(uri)
				.entity(newAutomovil)
				.build();
	}
	
	@DELETE
	@Path("/{automovilId}")
	public void deleteAutomovil(@PathParam("automovilId") long id) {
		automovilService.removeAutomovil(id);
	}
	
	@PUT
	@Path("/{automovilId}")
	public Automovil updateAutomovil(@PathParam("automovilId") long id, Automovil automovil) {
		automovil.setId(id);
		return automovilService.updateAutomovil(automovil);
	}	
	
	@Path("/{automovilId}/comments")
	public CommentResource getCommentResource() {
		return new CommentResource();
	}

	private String getUriForSelf(UriInfo uriInfo, Automovil automovil) {
		String uri = uriInfo.getBaseUriBuilder()
		 .path(AutomovilResource.class)
		 .path(Long.toString(automovil.getId()))
		 .build()
		 .toString();
		return uri;
	}
	
	private String getUriForComments(UriInfo uriInfo, Automovil automovil) {
		URI uri = uriInfo.getBaseUriBuilder()
				.path(AutomovilResource.class)
	       		.path(AutomovilResource.class, "getCommentResource")
	       		.path(CommentResource.class)
	       		.resolveTemplate("automovilId", automovil.getId())
	            .build();
	    return uri.toString();
	}
	
}
