package ibm.ws.poc.market.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ibm.ws.poc.market.model.Comment;
import ibm.ws.poc.market.service.CommentService;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CommentResource {

	private CommentService commentService = new CommentService();
	
	@GET
	public List<Comment> getAllComments(@PathParam("automovilId") long automovilId) {
		return commentService.getAllComments(automovilId);
	}
	
	@GET
	@Path("/{commentId}")
	public Comment getMessage(@PathParam("automovilId") long automovilId, @PathParam("commentId") long commentId) {
		return commentService.getComment(automovilId, commentId);
	}
	
	@POST
	public Comment addComment(@PathParam("automovilId") long automovilId, Comment comment) {
		return commentService.addComment(automovilId, comment);
	}
	
	@PUT
	@Path("/{commentId}")
	public Comment updateComment(@PathParam("automovilId") long automovilId, @PathParam("commentId") long id, Comment comment) {
		comment.setId(id);
		return commentService.updateComment(automovilId, comment);
	}
	
	@DELETE
	@Path("/{commentId}")
	public void deleteComment(@PathParam("automovilId") long automovilId, @PathParam("commentId") long commentId) {
		commentService.removeComment(automovilId, commentId);
	}
	
}
