package ibm.ws.poc.market.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import ibm.ws.poc.market.model.*;
import ibm.ws.poc.market.database.*;
import ibm.ws.poc.market.exception.DataNotFoundException;

public class AutomovilService {
	private AutomovilDBManager database;

	public AutomovilService() {
		database = new AutomovilDBManager();
	}

	
	public List<Automovil> getAllAutomovil() {
		return new ArrayList<Automovil>(database.select()); 
	}	
	

	public Automovil getAutomovil(long id) {
		Automovil automovil = database.select(id);
		if (automovil == null) {
			throw new DataNotFoundException("Automovil with id " + id + " not found");
		}
		return automovil;
	}

	public Automovil addAutomovil(Automovil automovil) {
		return database.insert(automovil);
	}
	
	public boolean removeAutomovil(long id) {
		return database.remove(id);
	}
	
	
	public Automovil updateAutomovil(Automovil automovil) {
		if (automovil.getId() <= 0) {
			return null;
		}
		database.update(automovil.getId(), automovil);
		return automovil;
	}
	
	public List<Automovil> getAllAutomovilForYear(int year) {
		List<Automovil> automovilForYear = new ArrayList<>();
		Calendar cal = Calendar.getInstance();
		for (Automovil automovil : getAllAutomovil()) {
			cal.setTime(automovil.getCreated());
			if (cal.get(Calendar.YEAR) == year) {
				automovilForYear.add(automovil);
			}
		}
		return automovilForYear;
	}
	
	public List<Automovil> getAllAutomovilPaginated(int start, int size) {
		ArrayList<Automovil> list = new ArrayList<Automovil>(getAllAutomovil());
		if (start + size > list.size()) return new ArrayList<Automovil>();
		return list.subList(start, start + size); 
	}	
}
