package ibm.ws.poc.market.service;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import ibm.ws.poc.market.database.CommentDBManager;
import ibm.ws.poc.market.model.Comment;
import ibm.ws.poc.market.model.ErrorMessage;

public class CommentService {
	private CommentDBManager database;

	
	public static void main(String[] args) {
		CommentDBManager manager = new CommentDBManager();
		manager.insert(1L, new Comment(1L, "Hola","Julio"));
	}
	
	
	public CommentService() {
		database = new CommentDBManager();
	}
	
	
	public List<Comment> getAllComments(long id_auto) {
		return new ArrayList<Comment>(database.select(id_auto));
	}
	
	public Comment getComment(long id_auto, long id) {
		ErrorMessage errorMessage = new ErrorMessage("No esta :S", 404, "http://google.com");
		Response response = Response.status(Status.NOT_FOUND)
				.entity(errorMessage)
				.build();
		Comment comment = database.select(id_auto, id);
		if (comment == null) {
			throw new NotFoundException(response);
		}
		return comment;
	}
	
	public Comment addComment(long id_auto, Comment comment) {
		database.insert(id_auto, comment);
		return comment;
	}
	
	public Comment updateComment(long id_auto, Comment comment) {
		if (comment.getId() <= 0) {
			return null;
		}
		database.update(id_auto, comment.getId(), comment);
		return comment;
	}
	
	public boolean removeComment(long id_auto, long id) {
		return database.remove(id);
	}
		
}
