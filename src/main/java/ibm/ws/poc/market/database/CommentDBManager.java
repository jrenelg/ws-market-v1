package ibm.ws.poc.market.database;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import ibm.ws.poc.market.model.Comment;

public class CommentDBManager {
	private final static String COMMENT_CREATE = SQLiteConnection.BASEPATH + "COMMENT_TABLE.sql";
	
	public static void createTable(Connection conn){
		try {File file = new File(COMMENT_CREATE);
			FileInputStream fis = new FileInputStream(file);
			byte[] data = new byte[(int) file.length()];
			fis.read(data);
			fis.close();
			String str = new String(data, "UTF-8");
			Statement stmt = conn.createStatement();
			if(stmt.execute(str)) {
				System.out.println("Comment table created");
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public ArrayList<Comment> select(long id_auto) {
		ArrayList<Comment> comments = new ArrayList<>();
		try{
			Statement stmt = SQLiteConnection.get().createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM COMMENT WHERE ID_AUTOMOVIL="+id_auto);
			while(rs.next()) {
				Comment comment = new Comment();
				comment.setId(rs.getInt("ID"));
				comment.setAuthor(rs.getString("AUTOR"));
				comment.setMessage(rs.getString("MENSAJE"));
				DateFormat format = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
				comment.setCreated(format.parse(rs.getString("CREATED")));
				comments.add(comment);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return comments;
	}
	
	public Comment select(long id_auto, long id) {
		Comment comment = new Comment();
		try{
			Statement stmt = SQLiteConnection.get().createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM COMMENT WHERE ID ="+id);
			if(rs.next()) {
				comment.setId(rs.getInt("ID"));
				comment.setAuthor(rs.getString("AUTOR"));
				comment.setMessage(rs.getString("MENSAJE"));
				DateFormat format = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
				comment.setCreated(format.parse(rs.getString("CREATED")));
			}else {
				return null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return comment;
	}
	
	public Comment insert(long id_auto, Comment comment) {
		try{
			PreparedStatement stmt = SQLiteConnection.get().prepareStatement("INSERT INTO COMMENT(ID_AUTOMOVIL,MENSAJE,AUTOR,CREATED) VALUES(?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
			stmt.setLong(1, id_auto);
			stmt.setString(2, comment.getMessage());
			stmt.setString(3, comment.getAuthor());
			Date current = new Date();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
			stmt.setString(4,dateFormat.format(current));
		    stmt.execute();
		    ResultSet rs = stmt.getGeneratedKeys();
		    if (rs.next()) {
		        long id = rs.getLong(1);
		        comment.setId(id);
		        comment.setCreated(current);
		    }
		} catch (SQLException e) {
			e.printStackTrace();
		}	
		return comment;
	}
	
	public boolean update(long id_auto, long id, Comment comment) {
		try{
			PreparedStatement stmt = SQLiteConnection.get().prepareStatement("UPDATE COMMENT SET MENSAJE=?, AUTOR=? WHERE ID_AUTOMOVIL=? AND ID = ?");
			stmt.setString(1, comment.getMessage());
			stmt.setString(2, comment.getAuthor());
			stmt.setLong(3, id_auto);
			stmt.setLong(4, id);
		    stmt.execute();
		    if(stmt.getUpdateCount() > 0){
				System.out.println("Automovil updated");
				return true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}	
		return false;
	}
	
	public boolean remove(long id) {
		try{
			Statement stmt = SQLiteConnection.get().createStatement();
			if(stmt.executeUpdate("DELETE FROM COMMENT WHERE ID = " + id) > 0 ) {
				System.out.println("Automovil with id " + id + " deleted");
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}	
		return false;
	}
	
}
