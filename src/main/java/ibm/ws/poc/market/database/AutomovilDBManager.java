package ibm.ws.poc.market.database;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import ibm.ws.poc.market.model.Automovil;

public class AutomovilDBManager {
	private final static String AUTOMOVIL_CREATE = SQLiteConnection.BASEPATH + "AUTOMOVIL_TABLE.sql";
	
	public static void createTable(Connection conn){
		try {File file = new File(AUTOMOVIL_CREATE);
			FileInputStream fis = new FileInputStream(file);
			byte[] data = new byte[(int) file.length()];
			fis.read(data);
			fis.close();
			String str = new String(data, "UTF-8");
			Statement stmt = conn.createStatement();
			if(stmt.execute(str)) {
				System.out.println("Automovil table created");
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public ArrayList<Automovil> select() {
		ArrayList<Automovil> automoviles = new ArrayList<>();
		try{
			Statement stmt = SQLiteConnection.get().createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM AUTOMOVIL");
			while(rs.next()) {
				Automovil automovil = new Automovil();
				automovil.setId(rs.getInt("ID"));
				automovil.setMarca(rs.getString("MARCA"));
				automovil.setModelo(rs.getString("MODELO"));
				automovil.setColor(rs.getString("COLOR"));
				DateFormat format = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
				automovil.setCreated(format.parse(rs.getString("CREATED")));
				automoviles.add(automovil);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return automoviles;
	}
	
	public Automovil select(long id) {
		Automovil automovil = new Automovil();
		try{
			Statement stmt = SQLiteConnection.get().createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM AUTOMOVIL WHERE ID ="+id);
			if(rs.next()) {
				automovil.setId(rs.getInt("ID"));
				automovil.setMarca(rs.getString("MARCA"));
				automovil.setModelo(rs.getString("MODELO"));
				automovil.setColor(rs.getString("COLOR"));
				DateFormat format = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
				automovil.setCreated(format.parse(rs.getString("CREATED")));
			}else {
				return null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return automovil;
	}
	
	public Automovil insert(Automovil automovil) {
		try{
			PreparedStatement stmt = SQLiteConnection.get().prepareStatement("INSERT INTO AUTOMOVIL(MARCA,MODELO,COLOR,CREATED) VALUES(?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, automovil.getMarca());
			stmt.setString(2, automovil.getModelo());
			stmt.setString(3, automovil.getColor());
			Date current = new Date();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
			stmt.setString(4,dateFormat.format(current));
		    stmt.execute();
		    ResultSet rs = stmt.getGeneratedKeys();
		    if (rs.next()) {
		        long id = rs.getLong(1);
		        automovil.setId(id);
		        automovil.setCreated(current);
		    }
		} catch (SQLException e) {
			e.printStackTrace();
		}	
		return automovil;
	}
	
	public boolean update(long id, Automovil automovil) {
		try{
			PreparedStatement stmt = SQLiteConnection.get().prepareStatement("UPDATE AUTOMOVIL SET MARCA=?, MODELO=?, COLOR=? WHERE ID = ?");
			stmt.setString(1, automovil.getMarca());
			stmt.setString(2, automovil.getModelo());
			stmt.setString(3, automovil.getColor());
			stmt.setLong(4, id);
		    stmt.execute();
		    if(stmt.getUpdateCount() > 0){
				System.out.println("Automovil updated");
				return true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}	
		return false;
	}
	
	public boolean remove(long id) {
		try{
			Statement stmt = SQLiteConnection.get().createStatement();
			if(stmt.executeUpdate("DELETE FROM AUTOMOVIL WHERE ID = " + id) > 0 ) {
				System.out.println("Automovil with id " + id + " deleted");
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}	
		return false;
	}
	
}
