package ibm.ws.poc.market.database;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SQLiteConnection {
	public final static String BASEPATH = "{YourPATH}\\ws-market-v1\\file\\";
	public final static String DBPATH = BASEPATH + "database.db";

	private static SQLiteConnection instance;
	private Connection conn = null;
	private boolean createTable = false;
	
	public static Connection get() {
		if (instance == null) {
			instance = new SQLiteConnection();
		}
		return instance.conn;
	}
	
	private SQLiteConnection(){
		try {
			File file = new File(DBPATH);
			if(!file.exists() && !file.isDirectory())
				createTable = true;
			String url = "jdbc:sqlite:"+DBPATH;
			Class.forName("org.sqlite.JDBC");
			conn = DriverManager.getConnection(url);
			System.out.println("Connection to SQLite has been established.");				
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		}
		if(createTable){
			AutomovilDBManager.createTable(conn);
			CommentDBManager.createTable(conn);
		}
	}
	
	@Override
	public void finalize(){
	    try {
	    	conn.close();
	    	System.out.println("Connection to SQLite has been closed.");
	    } catch (SQLException e) {
	    	System.out.println(e.getMessage());
	    }
	}

}
